package br.edu.ifpr.paranagua.concursomaneiro

import java.io.Serializable

class Participant (val title:String, val photographer:String, val date:String, var rates:ArrayList<Float>, var votes:Int = 0) : Serializable

fun Participant.getRating():Float {
    if (votes == 0) return 0.0f
    var average:Float = 0.0f
    for (rate in rates) {
        average += rate
    }
    average /= votes
    return average
}