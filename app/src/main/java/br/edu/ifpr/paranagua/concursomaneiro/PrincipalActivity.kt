package br.edu.ifpr.paranagua.concursomaneiro

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_principal.*

val participants:ArrayList<Participant> = getDataFromDAO()

class PrincipalActivity() : AppCompatActivity() {

    override fun onRestart() {
        super.onRestart()
        refreshRates()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)

        refreshRates()

        post_image_1.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 0)
            startActivity(intent)
        })
        post_image_2.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 1)
            startActivity(intent)
        })
        post_image_3.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 2)
            startActivity(intent)
        })
        post_image_4.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 3)
            startActivity(intent)
        })
        post_image_5.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 4)
            startActivity(intent)
        })
        post_image_6.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 5)
            startActivity(intent)
        })
        post_image_7.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 6)
            startActivity(intent)
        })
        post_image_8.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 7)
            startActivity(intent)
        })
        post_image_9.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 8)
            startActivity(intent)
        })
        post_image_10.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 9)
            startActivity(intent)
        })
        post_image_11.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 10)
            startActivity(intent)
        })
        post_image_12.setOnClickListener({
            val intent = Intent(this, RateActivity::class.java)
            intent.putExtra("participant_id", 11)
            startActivity(intent)
        })
    }

    fun refreshRates() {
        post_rate_1.rating = participants[0].getRating()
        post_rate_2.rating = participants[1].getRating()
        post_rate_3.rating = participants[2].getRating()
        post_rate_4.rating = participants[3].getRating()
        post_rate_5.rating = participants[4].getRating()
        post_rate_6.rating = participants[5].getRating()
        post_rate_7.rating = participants[6].getRating()
        post_rate_8.rating = participants[7].getRating()
        post_rate_9.rating = participants[8].getRating()
        post_rate_10.rating = participants[9].getRating()
        post_rate_11.rating = participants[10].getRating()
        post_rate_12.rating = participants[11].getRating()
    }
}

fun getDataFromDAO(): ArrayList<Participant> {
    return arrayListOf<Participant>(
            Participant("Pichu", "Treinador 01", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Vulpix", "Treinador 02", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Chikorita", "Treinador 03", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Victini", "Treinador 04", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Celebi", "Treinador 05", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Piplup", "Treinador 06", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Chespin", "Treinador 07", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Eevee", "Treinador 08", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Horsea", "Treinador 09", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Popplio", "Treinador 10", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Ralts", "Treinador 11", "15/04/2018", arrayListOf<Float>(0.0f), 0),
            Participant("Azurill", "Treinador 12", "15/04/2018", arrayListOf<Float>(0.0f), 0)
    )
}
