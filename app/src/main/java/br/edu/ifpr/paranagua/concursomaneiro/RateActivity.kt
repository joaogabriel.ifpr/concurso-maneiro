package br.edu.ifpr.paranagua.concursomaneiro

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_rate.*

class RateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate)

        var id:Int = getIntent().getIntExtra("participant_id", -1)
        var participant:Participant = participants[id]

        when (participant.title) {
            "Pichu" -> post_image.setImageResource(R.drawable.pichu)
            "Vulpix" -> post_image.setImageResource(R.drawable.vulpix)
            "Chikorita" -> post_image.setImageResource(R.drawable.chikorita)
            "Victini" -> post_image.setImageResource(R.drawable.victini)
            "Celebi" -> post_image.setImageResource(R.drawable.celebi)
            "Piplup" -> post_image.setImageResource(R.drawable.piplup)
            "Chespin" -> post_image.setImageResource(R.drawable.chespin)
            "Eevee" -> post_image.setImageResource(R.drawable.eevee)
            "Horsea" -> post_image.setImageResource(R.drawable.horsea)
            "Popplio" -> post_image.setImageResource(R.drawable.popplio)
            "Ralts" -> post_image.setImageResource(R.drawable.ralts)
            "Azurill" -> post_image.setImageResource(R.drawable.azurill)
        }
        post_title.setText(participant.title)
        post_photographer.setText("Fotografia por: ${participant.photographer}")
        post_date.setText("Tirada em: ${participant.date}")
        post_actual_rate.rating = participant.getRating()

        bt_vote.setOnClickListener({
            participant.rates.add(rating_bar.rating)
            participant.votes += 1
            participants[id] = participant
            this.finish()
            Toast.makeText(this, "Sucesso!", Toast.LENGTH_SHORT).show()
        })
    }
}
